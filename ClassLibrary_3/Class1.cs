﻿using System;

namespace ClassLibrary_3
{
    

    public class Class1
    {
        public double P;
        public double A;


        public static double Forml(double P, double A)
        {
            if (P <= 0)
            {
                throw new ArgumentException("Периметр не может быть 0");
            }
            if (A <= 0)
            {
                throw new ArgumentException("Апофема не может быть 0");
            }
            return (1d / 2d) * P * A;
        }
    }
}
