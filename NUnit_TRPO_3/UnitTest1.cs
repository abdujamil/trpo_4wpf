using ClassLibrary_3;
using NUnit.Framework;
using System;


namespace NUnit_TRPO
{
    public class Tests
    {

        [SetUp]
        public void Test1()
        {
        }

        [Test]
        public void Sq()
        {
            const double i = 8.3940300000000008d;
            double S = Class1.Forml(2.322, 7.23);
            Assert.AreEqual(i, S);
        }
        [Test]
       public void Testing()
       {
          Assert.Throws<ArgumentException>(() => Class1.Forml(-15, -4));
       }
    }
}